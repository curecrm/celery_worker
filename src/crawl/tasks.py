"""
Tasks for celery_worker.

All tasks in this project live in the celery_worker queue, not the default ("celery") queue.
This is set in celeryconfig.py.
"""
from celery import Celery
from celery.utils.log import get_task_logger

from utils import *
from celeryconfig import get_celery
import exchange, mail

import sys, os, re, json, pytz, shutil, tempfile, urllib, urllib2, email
import pdb, traceback

CALLBACK_NUM_BYTES = 1000000
CALLBACK_NUM_MSGS = 10

# Failure codes for the crawl
CRAWL_FAILURE_CONNECT   = 'failure_connect'
CRAWL_FAILURE_KEYBOARD  = 'failure_keyboard'
CRAWL_FAILURE_API       = 'failure_api'


logger = get_task_logger(__name__)
app = get_celery()


@app.task
def test_task(*args, **kwargs):
    "A way to test that celery is alive and works."
    print "CeleryWorkerTestTask:", args
    result = args[0] + 111
    print "Result is:", result
    return result


@app.task
def test_wait_task(*args, **kwargs):
    "Another way to test that celery is alive and works."

    print "CeleryWorkerTestWaitTask:", args
    import time
    time.sleep(15)
    return "Okay!"


@app.task
def mail_server_auth_task(mail_settings, **kwargs):
    """
    Check that a the given mail settings are valid by connecting to the mail server
    """
    logger.info("MailServerAuthTask start...")
    logger.info("MailServerAuthTask: stored info")

    server = exchange.MailServer.new_from_type(mail_settings)

    try:
        if 'google_captcha' in mail_settings:
            captcha = mail_settings['google_captcha']
            answer = mail_settings['google_captcha_answer']
            if not server.submit_captcha_answer(captcha, answer):
                raise Exception("Bad answer to Google Apps captcha")

        server.login()      # throws exception on failure
        server.logout()
        return {
            'success': True
        }

    except Exception, e:
        logger.info("MailServerAuthTask: failure")
        logger.info("Here's the failure:")
        logger.info(traceback.format_exc())
        exc_str = " -- mail settings:\n"
        exc_str += ''.join(['%s: %s\n' % (key, str(value)) for key, value in mail_settings.items() if ('password' not in key) and len(str(value)) < 50])
        # Include the original message's exception, it may contain a clue as to what went wrong
        #  GMail will say '[ALERT] Web login required (Failure)' if captcha needed
        logger.info(exc_str)
        return {                                 # Don't raise an exception -- apparently that doesn't work in our current version of Celery/RabbitMQ
            'success':  False,
            'error':    e.message + '\n' + exc_str
        }


    logger.info("MailServerAuthTask: success")


@app.task
def get_captcha_task(mail_settings, **kwargs):
    """
    Retrieve a captcha challenge from a mail server (only Google Mail / Google Apps for now)
    """
    logger.info("GetCaptchaTask start...")

    server = exchange.MailServer.new_from_type(mail_settings)
    captcha = server.get_captcha()

    logger.info("GetCaptchaTask: success")
    return captcha


@app.task
def crawl_task(*args, **kwargs):
    logger.info('craw_task %s %s' % (args, kwargs))
    return CrawlTask().run(*args, **kwargs)


@app.task
def retrieve_and_store_file_attachment_task(*args, **kwargs):
    return RetrieveAndStoreFileAttachmentTask().run(*args, **kwargs)


CALLBACK_NUM_RETRIES = 4
CALLBACK_RETRY_INTERVAL = 60


class CrawlTask(object):
    """
    Crawl recent mails from a mail account.

    Call a callback API with a JSON structure giving the returned mail messages and a snippet from each.
    """

    def _callback(self, callback_url, data):
        '''
        Helper function to do the actual callback.
        Datetimes get encoded with the DateTimeJSONEncoder.
        '''

        for i in range(CALLBACK_NUM_RETRIES):
            try:
                logger.info(" - calling API: ", callback_url)
                request = urllib2.Request(callback_url, DateTimeJSONEncoder().encode(data))
                request.add_header('Content-Type', 'application/json')  # text/json doesn't work with Piston
                handle = urllib2.urlopen(request)
                result_data = json.loads(handle.read())
                return result_data['success']
            except:
                logger.info(" - calling API, try %d: failure" % i)
                timemodule.sleep(CALLBACK_RETRY_INTERVAL)

        logger.info(" - calling API: giving up")
        return False

    def callback_data_success(self, callback_url, callback_struct, message_dicts, folders, num_fetched_msgs, num_total_msgs, done):
        '''
        Call the callback API with a JSON dictionary of crawled messages ready to be stored in the DB
        '''

        payload = {
            'success':              True,
            'done':                 done,
            'callback_struct':      callback_struct,
            'message_dicts':        message_dicts,
            'num_fetched_msgs':     num_fetched_msgs,
            'num_total_msgs':       num_total_msgs,
            'folders':              folders
        }
        logger.info('callback_data_success: %s %s' % (callback_url, payload))
        return self._callback(callback_url, payload)

    def callback_data_failure(self, callback_url, callback_struct, failure_reason):
        '''
        Call the callback API with a failure code (for example, can't connect to server)
        '''

        return self._callback(callback_url, {
            'success':              False,
            'callback_struct':      callback_struct,
            'failure_reason':       failure_reason
        })

    def is_processable_message(self, message_dict, block_mailing_list_mails=False, block_confidential_mails=False):
        '''
        Return true if this message should actually be processed.
        False if:
            - message is from a mailing list (if block_mailing_list_mails is True)
            - message contains "Confidential" or similar in the subject line (if block_confidential_mails is True)
            - message is from a mailer-daemon or looks like a bounce message
        '''

        if not message_dict['from']:
            return False

        if block_mailing_list_mails:
            if ('List-Unsubscribe' in message_dict['headers']) or \
                (message_dict['headers'].get('Precedence', '').lower() == 'bulk'):
                return False

        if block_confidential_mails:
            if RE_CONFIDENTIAL.search(message_dict['subject_normalized']):
                return False

        if is_mailer_daemon(message_dict['from']['addr']):
            return False

        if RE_DELIVERY.search(message_dict['from']['name']):
            return False

        if RE_DELIVERY.search(message_dict['subject']):
            return False

        return True


    def run(self, mail_settings, since, callback_url, **kwargs):

        '''
        Required parameters:
        mail_settings - a dictionary of mail credentials
        since - crawl messages going back to this date
        callback_url - a URL to POST to with the resulting messages

        Optional parameters:
        max_msgs - max # of recent messages to crawl per folder (default None, meaning crawl all messages since "since")
        previous_folders - a list of previously crawled mail folders for this user.
            When crawling a mail folder which is in this list, the mail crawler will only crawl
            messages starting after the last message that was already stored,
            The dictionary is keyed by name.
            Each folder is represented as a dictionary in this format:
                'name':             name of the folder
                'highest_crawled'
                'highest_path'
                'highest_date'
        callback_num_bytes - total length, in bytes, of the messages we accumulate in memory before we call the callback
        callback_num_msgs - max # of messages we accumulate in memory before we call the callback routine
        block_confidential_mails - if True, ignore mails that have "Confidential" (or similar) in subject line - default False
        block_mailing_list_mails - if True, ignore mails that appear to be from a mailing list - default False
        archive_mode - if set, crawl all folders and not just the top-level ones
        user_email - a handy identifier, used for the name of the
            log file and the temporary file (default is '')
        callback_struct - a json dictionary or other json-friendly object to be passed back to the callback URL,
            the callback API uses this for its own purposes (e.g. identifying which alias these messages belong to)
        '''

        user_email = kwargs.get('user_email', '')

        with TaskLogFile(self, user_email):
            logger.info("CrawlTask start:")
            for key, value in mail_settings.items():        # Log the arguments we were called with
                if 'password' not in key.lower():
                    logger.info(" - %s = %s" % (key, value))
            for key, value in kwargs.items():
                if 'password' not in key.lower():
                    logger.info(" - %s = %s" % (key, value))

            max_msgs = kwargs.get('max_msgs')
            previous_folders = kwargs.get('previous_folders', [])
            archive_mode = kwargs.get('archive_mode', False)
            callback_num_bytes = kwargs.get('callback_num_bytes', CALLBACK_NUM_BYTES)
            callback_num_msgs = kwargs.get('callback_num_msgs', CALLBACK_NUM_MSGS)
            callback_struct = kwargs.get('callback_struct')
            block_confidential_mails = kwargs.get('block_confidential_mails', False)
            block_mailing_list_mails = kwargs.get('block_mailing_list_mails', False)

            # Log into the server

            logger.info(" Logging into server...")              # TODO: NotedTaskMeta
            try:
                server = exchange.MailServer.new_from_type(mail_settings)
                server.login()
            except:
                logger.info(" error: can't log in to server")
                self.callback_data_failure(callback_url, callback_struct, CRAWL_FAILURE_CONNECT)
                return {'success': False, 'failure_reason': CRAWL_FAILURE_CONNECT}

            # Get list of folders
            logger.info(" Getting list of folders...")          # TODO: NotedTaskMeta
            folders = server.get_folder_list()

            # If any of the folders are special "all mail" folders (like Gmail), crawl only those
            if any (f['all_mail'] for f in folders):
                folders = [f for f in folders if f['all_mail']]

            # If we have previously crawled any of these folders, make sure we know which was the last email
            #  we crawled in each folder (so we can start from there)
            for f in folders:
                for p in previous_folders:
                    if p['name'] == f['name']:
                        f.update(p)                         # update highest_crawled, highest_path, highest_date

            # If too many folders, just keep the main ones
            #  (In archiving mode, crawl everything)
            if (len(folders) > HOWMANY_IMAP_FOLDERS_MAX) and not archive_mode:
                folders = [f for f in folders if ('/' not in f['name']) or re.search(r'(?i)(gmail|google mail)', f['name'])]

            # Get the IDs of the messages from all folders, and a message count for the progress bar
            #  fetched_msgs_list is of the form:
            #   [ (folder_name, uid), (folder_name, uid), ... ]

            fetched_msgs_list = []
            num_total_msgs = 0

            for folder in folders:
                try:
                    logger.info(' - folder: ', folder['name'])

                    if (folder.get('highest_path') or folder.get('highest_date')) and not archive_mode:
                        # Crawl the folder starting after the last message that was crawled
                        # Ignore max_msgs in this case
                        msg_tuples = server.select_folder(folder, highest_path=folder['highest_path'], highest_date=folder['highest_date'])
                    else:
                        # First time crawling this folder, just crawl the last max_msgs messages newer than the date "since"
                        msg_tuples = server.select_folder(folder, since=since, max_msgs=max_msgs)

                    for path, dt in msg_tuples:
                        fetched_msgs_list.append((folder, path))
                        num_total_msgs += 1

                except:
                    logger.info("Can't select folder %s" % folder['name'])

            # Actually fetch the messages, and pre-process them inside of this crawler
            # Accumulate messages in memory until we either have some number of them or
            # the # of bytes exceeds some threshold -- when that happens, call the API and get rid of them

            current_folder = None
            num_fetched_msgs = 0                # Total number of fetched messages

            message_dicts_this_batch = []       # A list of message_dicts, when they're too big then dump them

            for folder, path in fetched_msgs_list:
                if folder != current_folder:
                    server.select_folder(folder, since=since, get_tuples=False)
                    current_folder = folder

                if folder.get('deleted'):       # Don't crawl folders of deleted messages
                    continue

                # If we get here, there's at least one message, so no divide-by-zero issue
                #percent_done = int((num_fetched_msgs/float(len(fetched_msgs_list)))*50.0)           # This is the first half of the crawl

                try:
                    logger.info(' - email: %s: UID %s' % (folder['name'], path))
                    raw_message, flags, file_placeholders = server.fetch_raw_message(path)

                    message_dict = mail.message_dict_from_raw(raw_message, folder=folder, path=path, dt=None, flags=flags,
                        file_placeholders=[f.as_dict() for f in file_placeholders])

                    if self.is_processable_message(message_dict, block_mailing_list_mails=block_mailing_list_mails, block_confidential_mails=block_confidential_mails):
                        log (' -    subject: ', message_dict['subject'])
                        message_dicts_this_batch.append(message_dict)
                    else:
                        log (' -    IGNORING subject: ', message_dict['subject'])

                    if (folder.get('highest_date') is None) or folder['highest_date'] < message_dict['date_sent']:
                        folder['highest_date'] = message_dict['date_sent']
                        folder['highest_path'] = path

                    num_fetched_msgs += 1

                except KeyboardInterrupt:                       # For debugging, bail out with Ctrl-C
                    return {'success': False, 'failure_reason': CRAWL_FAILURE_KEYBOARD}

                except:
                    logger.info(traceback.format_exc())
                    logger.info("Warning: couldn't retrieve message UID%s from folder %s" % (path, str(folder)))

                # If we've accumulated enough messages in memory (or their total size is too big),
                #  call the callback API to process these messages, then get rid of them from our memory
                if message_dicts_this_batch and (len(message_dicts_this_batch) >= callback_num_msgs or sum(m['len'] for m in message_dicts_this_batch) >= callback_num_bytes):
                    # Call the API to unload these messages
                    if not self.callback_data_success(callback_url, callback_struct, message_dicts_this_batch, folders, num_fetched_msgs, num_total_msgs, False):
                        return {'success': False, 'failure_reason': CRAWL_FAILURE_API}       # API failed? Then propagate the failure
                    message_dicts_this_batch = []

            # Call the API to unload the last batch of messages and indicate that we're done
            #  (Do this even if there are no messages)
            self.callback_data_success(callback_url, callback_struct, message_dicts_this_batch, folders, num_fetched_msgs, num_total_msgs, True)
            message_dicts_this_batch = []

            logger.info(" Logging out of server...")              # TODO: NotedTaskMeta
            server.logout()
            logger.info(" ... CrawlTask DONE!")

            return {'success': True}


class RetrieveAndStoreFileAttachmentTask(object):
    """
    Retrieve a file attachment from a mail stored somewhere on this user's server, store it on Cloud Files
    """

    def run(self, mail_settings, folder, path, mimepath, container_name, file_name, **kwargs):
        """
        Retrieve a file attachment from a mail stored somewhere on this user's server, and
        store it on Cloud Files. Returns the path to the file on Cloud Files starting with "mosso:",
        or None if it could not retrieve the file attachment (message expired on IMAP).

        mail_settings: a structure descrbing the user's mail server and password (same as in CrawlTask above)
        folder: a structure describing the user's mail folder
        path: The path (for IMAP, the UID) of the specific message within that folder
        mimepath: The MIME path descriptor (example: "1.3.2") of the MIME part containing the file attachment within that message
        """

        from lib.cloud import CloudFilesManager
        import email.Parser

        with TaskLogFile(self, kwargs.get('task_id')):
            logger.info("RetrieveAndStoreFileAttachmentTask start...")

            server = exchange.MailServer.new_from_type(mail_settings)

            try:
                server.login()      # throws exception on failure

                try:
                    server.select_folder(folder['name'])
                except:
                    # In some cases (Outlook Web Access), select folder won't work and isn't necessary
                    pass

                raw_message_part, encoded = server.fetch_raw_message_part(path, mimepath)

                if encoded:
                    # Turn it from a MIME section into an actual file (IMAP)
                    part = email.Parser.Parser().parsestr(raw_message_part)
                    s = part.get_payload(decode=True)
                else:
                    # Raw binary blob (OWA)
                    s = raw_message_part

                s = part.get_payload(decode=True)
                server.logout()

                # Store the file on the cloud
                cloud_manager = CloudFilesManager()
                file_path = cloud_manager.save_file(s, container_name, file_name)

                logger.info("RetrieveAndStoreFileAttachmentTask: success, file available at %s" % file_path)
                return {
                    'success': True,
                    'file_path': file_path
                }

            except Exception, e:
                logger.info("RetrieveAndStoreFileAttachmentTask: failure (message expired, or mail settings bad)")
                logger.info(traceback.format_exc())
                return {                                 # Don't raise an exception -- apparently that doesn't work in our current version of Celery/RabbitMQ
                    'success':  False,
                }
