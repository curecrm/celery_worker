from utils import *
rein = reinterpret_by_tz            # shorthand for this utility function which we use everywhere in this file

# Symbolic constants used internally by the parser
# The strings are for debugging only, do NOT internationalize them

PD_LAST =       ':last'
PD_THIS =       ':this'
PD_NEXT =       ':next'
PD_EVERY =      ':every'

PD_PAST =       ':past'
PD_FUTURE =     ':future'

PD_COMMA =      ':comma'
PD_SLASH =      ':slash'
PD_DASH =       ':dash'
PD_AT =         ':at'
PD_FROM =       ':from'
PD_IN =         ':in'
PD_OF =         ':of'
PD_ON =         ':on'
PD_BY =         ':by'
PD_AND =        ':and'
PD_OR =         ':or'
PD_BETWEEN =    ':between'
PD_UNTIL =      ':until'
PD_THROUGH =    ':through'

PD_JANUARY =    ':january'
PD_FEBRUARY =   ':february'
PD_MARCH =      ':march'
PD_APRIL =      ':april'
PD_MAY =        ':may'
PD_JUNE =       ':june'
PD_JULY =       ':july'
PD_AUGUST =     ':august'
PD_SEPTEMBER =  ':september'
PD_OCTOBER =    ':october'
PD_NOVEMBER =   ':november'
PD_DECEMBER =   ':december'

PD_MONDAY =     ':monday'
PD_TUESDAY =    ':tuesday'
PD_WEDNESDAY =  ':wednesday'
PD_THURSDAY =   ':thursday'
PD_FRIDAY =     ':friday'
PD_SATURDAY =   ':saturday'
PD_SUNDAY =     ':sunday'

PD_AM =         ':am'
PD_PM =         ':pm'
PD_MORNING =    ':morning'
PD_AFTERNOON =  ':afternoon'
PD_EVENING =    ':evening'
PD_NIGHT =      ':night'

PD_YEAR =       ':year'
PD_SEASON =     ':season'
PD_MONTH =      ':month'
PD_FORTNIGHT =  ':fortnight'
PD_WEEK =       ':week'
PD_WEEKEND =    ':weekend'
PD_DAY =        ':day'
PD_HOUR =       ':hour'
PD_MINUTE =     ':minute'
PD_SECOND =     ':second'


class Token(object):
    
    def __init__(self, word, index=None):
        self.orig_word = word
        self.tags = []
        self.word = self.orig_word.lower()
        if len(self.word) >= 2 and self.word[0] == '@':
            self.word = self.word[1:]
        self.index = index
        self.heat = 0
        self.peak_type = CHRONIC_PEAK_NONE                  # This token, if it's a peak, is this kind of peak
    
    def get_tag(self, tag_class):
        matches = [tag for tag in self.tags if isinstance(tag, tag_class)]
        if len(matches) > 0:
            return matches[0]
        else:
            return None
        
    def untag(self, tag_class):
        self.tags = [tag for tag in self.tags if not isinstance(tag, tag_class)]
                        
    def tag(self, new_tag):
        if new_tag:                         # Don't append tag if it's None
            self.tags.append(new_tag)    
        
    def reset(self):
        for t in self.tags:
            if t:
                t.reset()
            
    def __repr__(self):
        return u'(%s:%f %s)' % (self.word, self.heat, repr(self.tags))
       
        
class Tag(object):
    def __init__(self, data=None):
        self.data = data
        self.fake = False
        
    def start(self, now):        
        self.now = now
        
    def reset(self):
        pass                                # Override this in the classes

    def get_within_min_peak_type(self):
        if hasattr(self, 'within_min_peak_type'):
            return self.within_min_peak_type
        return CHRONIC_PEAK_REGULAR
    
class Span(object):
    
    NONE        = 0
    TIME        = 1 << 0
    DAYPORTION  = 1 << 1
    DATE        = 1 << 2
    
    DATE_TIME   = DATE | TIME
        
    def __init__(self, start, end=None, time=False, type=None):
        if end is None:
            self.start = start
            self.end = start
        else:
            self.start = start
            self.end = end
        self.rule = None
        self.strong = False
        self.type = type
        self.tokens = []                        # the Chronic tokens making up this span

    def width(self):
        return self.end - self.start            # usually, a timedelta

    def includes(self, other):
        if type(other) is Span:
            return (self.start <= other.start and other.end <= self.end)
        else:
            return (self.start <= other and other <= self.end)
        
    def intersects(self, other_span):
        if other_span.start <= self.start and other_span.end <= self.start:
            return False
        if other_span.start >= self.end and other_span.end >= self.end:
            return False
        return True            
        
    def valid(self):
        return (self.start <= self.end)
    
    def is_date_only(self):
        if self.type is not None:
            return self.type == Span.DATE
        else:
            return is_all_day(self.start, self.end)
        
    def is_time_only(self):
        if self.type is not None:
            return (self.type & Span.DATE) == 0
        else:
            return self.width() < timedelta(days=1)
    
    def combine_with_time(self, time_span):        
        self.start = rein(self.start.replace(\
            hour=time_span.start.hour, minute=time_span.start.minute, second=time_span.start.second, microsecond=time_span.start.microsecond))
        self.end = rein(self.start + (time_span.end - time_span.start))
        self.type |= Span.TIME
        
    def combine_with_date(self, date_span):        
        start = rein(date_span.start.replace(\
            hour=self.start.hour, minute=self.start.minute, second=self.start.second, microsecond=self.start.microsecond))
        end = rein(start + (self.end - self.start))  
        self.start, self.end = start, end
        self.type |= Span.DATE
        
    def __add__(self, other):                   # other is usually a timedelta
        return Span(rein(self.start+other), rein(self.end+other))
        
    def __eq__(self, other):
        # These are the only fields that matter for equality
        return self.start == other.start and self.end == other.end
    
    def __ne__(self, other):
        return not(self.__eq__(other))
        
    def __repr__(self):
        if self.start == self.end:
            return '<Span: %s to same%s>' % (unicode(self.start), ' R' if self.rule else '')
        else:
            return '<Span: %s to %s%s>' % (unicode(self.start), unicode(self.end), ' R' if self.rule else '')

    def get_rule(self):
        from schedule.models import Rule
        
        """
        Generate (but don't save to DB) a Rule object from self.rule
        """
        
        if self.rule and 'frequency' in self.rule:
            r = Rule(frequency = self.rule['frequency'])
            r.name = self.rule['frequency'].capitalize()    # TODO: Better human-friendly description of rule?
            r.description = self.rule.get('description', None)
            params = []
            for key, value in self.rule.items():
                if key in ['frequency', 'name', 'description']:
                    continue
                if type(value) == list:
                    valstr = ','.join([str(v) for v in value])
                else:
                    valstr = str(value)
                params.append('%s:%s' % (key, valstr))
            r.params = ';'.join(params)
            return r
        else:
            return None
        
       
class SpanSet(object):
    """A collection of related Span objects."""

    # TODO: Are they glued together by And or Or?

    def __init__(self, spans=None):
        if spans:
            self.spans = spans
        else:
            self.spans = []
             
    def __repr__(self):
        return '<SpanSet: %s>' % ''.join([repr(s) for s in self.spans])

                 
            