from htmlentitydefs import name2codepoint as n2cp
import email.utils
import email.header
import pdb, traceback
from datetime import *
import celery.task, celery.result
import celeryconfig

from constants import *
import pytz
import sys, os, random, re, hashlib, string, calendar, string, StringIO, copy, urllib, urllib2, json
import time as timemodule
from rfc822 import AddressList  # todo: use email.Utils.getaddresses - this is deprecated
from email.Utils import parsedate_tz, mktime_tz
rootdir = os.path.dirname(os.path.abspath(__file__))
datadir = os.path.join(rootdir, 'data')

# Some useful classes for profiling

class TimeClass():
    def __init__(self):
        self.created = now()
        self.times = []
    
    def get_total(self):
        total = timedelta(0)
        for dt in self.times:
            total += dt
        return total
                
    def get_num(self):
        return len(self.times)
  
    def get_avg(self):
        return self.get_total() / self.get_num()
    
    def get_max(self):
        return max(self.times)
    
    def get_min(self):
        return min(self.times)
    

class TimeStats():
      
    def __init__(self):
        self.times = {}
        self.time_starts = {}
        
    def start(self, key):
        self.time_starts[key] = now()
    
    def end(self, key):
        if self.time_starts.get(key, None) is None:
            raise Exception()
        if key not in self.times:
            self.times[key] = TimeClass()
        self.times[key].times.append(now() - self.time_starts[key])
        del self.time_starts[key]
        
    def __str__(self):
        keys = self.times.keys()
        keys.sort(key=lambda k: self.times[k].get_avg())
        result = "****** Time stats:\n"
        for key in keys:
            times = self.times[key]
            result += " -- %s: %s, avg=%s across %d insts, with max=%s and min=%s\n" % \
                (key, times.get_total(), times.get_avg(), times.get_num(), times.get_max(), times.get_min())
        return result



class TaskLogFile(object):
    def __init__(self, task, task_id=None):
        self.filename = '%s.%s.log' % (task.__class__.__name__, task_id or '')

    def __enter__(self):
        pass

    def __exit__(self, type, value, traceback):
        pass


class DateTimeJSONEncoder(json.JSONEncoder):
    """Specialized JSON encoder that can take datetime objects and format them according to this specification:
        %Y-%m-%d %H:%M:%S
    """
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        return json.JSONEncoder.default(self, obj)
    
    
# from http://github.com/sku/python-twitter-ircbot/blob/321d94e0e40d0acc92f5bf57d126b57369da70de/html_decode.py
def decode_htmlentities(string):
    
    def substitute_entity(match):
        ent = match.group(3)
        if match.group(1) == "#":
            # decoding by number
            if match.group(2) == '':
                # number is in decimal
                return unichr(int(ent))
            elif match.group(2) == 'x':
                # number is in hex
                return unichr(int('0x'+ent, 16))
        else:
            # they were using a name
            cp = n2cp.get(ent)
            if cp: return unichr(cp)
            else: return match.group()
    
    entity_re = re.compile(r'&(#?)(x?)(\w+);')
    return entity_re.subn(substitute_entity, string)[0]

    
def strip_nonascii(s):
    # From http://mail.python.org/pipermail/python-list/2004-February/249119.html
    # Much faster than using a join like the other strip functions below
    # (speed is important, this gets run over entire emails w/big attachments)
    try:    
        table = string.maketrans('', '')
        return string.translate(str(s), table, table[128:])
    except:
        return ''.join(i for i in s if ord(i) < 128)
        
def strip_nonprintable(s):
    # Zap the "gremlins" (weird ASCII control characters) from a string
    return ''.join(i for i in s if ord(i) >= 128 or i in string.printable)
    
def strip_nonalnum(s):
    return ''.join(i for i in s if i.isalnum())

def trim_nonalnum(s):
    s = s.strip()
    while s and not s[0].isalnum():
        s = s[1:]
    while s and not s[-1].isalnum():
        s = s[:-1]
    return s
        
def strip_nonalpha(s):
    return ''.join(i for i in s if i.isalpha())


def strip_nonname(s):
    return ''.join(i for i in s if i.isalpha() or i in "-'.")

# This is for registration and the assistant email address
def strip_nonalnumunderscore(s):
    return ''.join(i for i in s if i in (string.ascii_letters + string.digits + '_'))

def strip_nondigits(s):
    return ''.join(i for i in s if i in string.digits)

def strip_spaces(s):
    return ''.join(s.split())

def strip_linebreaks(s):
    "Useful for dealing with multi-line headers in incoming emails"
    return ''.join(i for i in s if (i==' ') or (not i.isspace()))

# From the Python email.headers library, but the requirement for whitespace/end of string
# at the end has been removed [we got some emails that weren't that way]
ECRE = re.compile(r'''
  =\?                   # literal =?
  (?P<charset>[^?]*?)   # non-greedy up to the next ? is the charset
  \?                    # literal ?
  (?P<encoding>[qb])    # either a "q" or a "b", case insensitive
  \?                    # literal ?
  (?P<encoded>.*?)      # non-greedy up to the next ?= is the encoded string
  \?=                   # literal ?=
  ''', re.VERBOSE | re.IGNORECASE | re.MULTILINE)

# Copied from Python email.headers.library, but we don't strip spaces 
def _decode_header_unicode(header):
    """Decode a message header value without converting charset.

    Returns a list of (decoded_string, charset) pairs containing each of the
    decoded parts of the header.  Charset is None for non-encoded parts of the
    header, otherwise a lower-case string containing the name of the character
    set specified in the encoded string.

    An email.Errors.HeaderParseError may be raised when certain decoding error
    occurs (e.g. a base64 decoding exception).
    """
    # If no encoding, just return the header
    header = str(header)
    if not ECRE.search(header):
        return [(header, None)]
    decoded = []
    dec = ''
    for line in header.splitlines():
        # This line might not have an encoding in it
        if not ECRE.search(line):
            decoded.append((line, None))
            continue
        parts = ECRE.split(line)
        while parts:
            unenc = parts.pop(0)
            unenc_stripped = unenc.strip()
            if unenc_stripped:
                # Should we continue a long line?
                if decoded and decoded[-1][1] is None:
                    decoded[-1] = (decoded[-1][0] + SPACE + unenc_stripped, None)
                else:
                    decoded.append((unenc, None))
            if parts:
                charset, encoding = [s.lower() for s in parts[0:2]]
                encoded = parts[2]
                dec = None
                if encoding == 'q':
                    dec = email.quoprimime.header_decode(encoded)
                elif encoding == 'b':
                    try:
                        dec = email.base64mime.decode(encoded)
                    except binascii.Error:
                        # Turn this into a higher level exception.  BAW: Right
                        # now we throw the lower level exception away but
                        # when/if we get exception chaining, we'll preserve it.
                        raise HeaderParseError
                if dec is None:
                    dec = encoded

                if decoded and decoded[-1][1] == charset:
                    decoded[-1] = (decoded[-1][0] + dec, decoded[-1][1])
                else:
                    decoded.append((dec, charset))
            del parts[0:3]
    return decoded


def decode_header_unicode(header):
    # From http://mail.python.org/pipermail/python-list/2009-February/702511.html
    if header is None:
        return None
    try:
        email.header.ecre = ECRE        # Some brain surgery -- patch the Python library
        return strip_nonprintable(''.join([s.decode(t or 'ascii') for (s, t) in _decode_header_unicode(header)]))
    except:
        return strip_nonprintable(header)
    
def encode_header_unicode(s):    
    if all(ord(c) < 128 for c in s):
        return s
    else:
        try:
            h = email.header.Header(s.encode('utf-8'), 'utf-8')
            return str(h)
        except:
            return strip_nonascii(s)

# These are like email.utils.parseaddr and email.utils.formataddr, but do the Unicode encoding/decoding

def parseaddr_unicode(s):
    name, addr = email.utils.parseaddr(s)
    name = decode_header_unicode(name)
    addr = addr.strip().lower()
    return name, addr

def formataddr_unicode(tup):
    name, addr = tup
    name = encode_header_unicode(name)
    return email.utils.formataddr((name, addr))

            
def get_name_addr_list(headers, key):
    '''
    Given a dictionary of an rfc822 email's headers, retrieve all the (name, addr) tuples
    corresponding to some key (where key = 'To', 'Cc' etc.)
    '''
    if key in headers:
        l = list(AddressList(headers[key]))
        l = [(decode_header_unicode(name), addr) for name, addr in l]
        return l
    else:
        return []
        
def get_part_filename(part):
    return decode_header_unicode(part.get_filename())
    
def strip_header_newlines(header):
    "Newlines, etc. aren't allowed in email headers, strip them before sending"
    return re.sub(r'(\t|\n|\r)+', ' ', header)

def is_valid_email_address(address):
    return len(address) <= MAX_EMAIL_ADDRESS_LENGTH and bool(RE_EMAIL.match(address.strip().lower()))

# ============================================================
# Debugging functions

DEBUG_INIT  =   True
DEBUG_ITER  =   True

def print_init_start(obj):
    if DEBUG_INIT:
        log("Start %s.__init__()" % type(obj))

def print_init_end(obj):
    if DEBUG_INIT:
        log("End   %s.__init__()" % type(obj))

def print_iter_start(obj):
    if DEBUG_ITER:
        log("Start %s.__iter__()" % type(obj))

def print_iter_end(obj):
    if DEBUG_ITER:
        log("End   %s.__iter__()" % type(obj))


def log(*args, **kwargs):
    try:
        sep = kwargs.get('sep', ' ')
        end = kwargs.get('end', '\n')
        f = kwargs.get('file', celeryconfig.LOG_FILE)
        level = kwargs.get('level', None)
        if f:
            timestamp = now().astimezone(SERVER_TIMEZONE).strftime("%m/%d %H:%M:%S")
            message = sep.join([u'%s' % str(item) for item in args]) + end
            f.write('%s> %s' % (timestamp, message))
            f.flush()
    except:
        pass
        


# ============================================================
# Date/time functions

ONE_SECOND =    timedelta(seconds=1)
ONE_MINUTE =    timedelta(minutes=1)
ONE_HOUR =      timedelta(hours=1)
ONE_DAY =       timedelta(days=1)
ONE_WEEKEND =   timedelta(days=2)
ONE_WEEK =      timedelta(days=7)

def now():
    """Return the current time in UTC, as a datetime with tzinfo=UTC"""
    return pytz.UTC.localize(datetime.utcnow())

def minute_start(datetime):
    return reinterpret_by_tz(datetime.replace(second=0, microsecond=0))

def hour_start(datetime):
    return reinterpret_by_tz(datetime.replace(minute=0, second=0, microsecond=0))
     
def day_start(datetime):
    """Return a datetime with the time components set to 00:00:00
    Date and tzinfo portions are left alone"""
    return reinterpret_by_tz(datetime.replace(hour=0, minute=0, second=0, microsecond=0))

def month_start(datetime):
    return reinterpret_by_tz(datetime.replace(day=1, hour=0, minute=0, second=0, microsecond=0))
    
def year_start(datetime):
    return reinterpret_by_tz(datetime.replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0))

def round_nearest_day(dt, threshold=3*ONE_HOUR):
    if dt - day_start(dt) <= threshold:
        return day_start(dt)
    elif reinterpret_by_tz(day_start(dt) + ONE_DAY) - dt <= threshold:
        return reinterpret_by_tz(day_start(dt) + ONE_DAY)
    else:
        return dt
    
    
def is_all_day(dtstart, dtend):
    """Determines whether a time span is an 'all day' event
    (starts at midnight, goes to following midnight)"""
    
    return (dtstart == day_start(dtstart)) and (dtend == reinterpret_by_tz(dtstart + ONE_DAY))

#def is_all_days(dtstart, dtend):
#    return (dtstart == day_start(dtstart)) and (dtend == day_start(dtend)) and (dtend > dtstart)

def is_all_week(dtstart, dtend):
    return (dtstart == day_start(dtstart)) and (dtend == reinterpret_by_tz(dtstart + ONE_WEEK))

def is_all_month(dtstart, dtend):
    if not (dtstart == day_start(dtstart)) and (dtend == day_start(dtend)):
        return False
    if not (dtstart.day == 1) and (dtend.day == 1) and (dtend > dtstart):
        return False
    if (dtend.year == dtstart.year) and (dtend.month == dtstart.month + 1):
        return True
    if (dtend.year == dtstart.year+1) and (dtend.month == 1) and (dtstart.month==12):
        return True
    return False

def find_day_portion(dtstart, dtend):
    if (dtend > dtstart) and ((dtend-dtstart) < ONE_DAY):
        for dp in DAY_PORTIONS:
            start_hour = dtstart.hour
            end_hour = dtend.hour + (24 if dtend.hour <= dtstart.hour else 0)
            if (start_hour, end_hour) == dp['hours']:
                return dp
    return None
    
def combine_date_time(dt_date, dt_time):    
    return dt_date.replace(hour=dt_time.hour, minute=dt_time.minute, second=dt_time.second, microsecond=dt_time.microsecond)
    
def ago(howlong):
    """Return a time which is "howlong" ago from the current time, as a datetime with tzinfo=UTC"""
    return now() - howlong

def later(howlong):
    """Return a UTC time which is "howlong" later than the current time, as a datetime with tzinfo=UTC"""
    return now() + howlong
    
def datetime_older_than(dt, howlong):
    """Determine if a datetime is older than a certain amount
    dt must be timezone-aware!"""
    if not dt: return True                  # dt=None indicates something needs to be updated for the first time
    if not howlong: return True             # howlong=None means any datetime is older than this (i.e., always update)
    return ((now() - dt) > howlong)
    
def query_older_than(query, howlong, field='date'):
    """Determine if a whole query needs to be updated, based on its oldest last-modified date"""
    if query.count() == 0: return True
    if datetime_older_than(getattr(query.order_by('-'+field)[0], field), howlong): return True
    return False

def unescape_ics_tzname(name):    
    # Unescape quoted chars, for example, r"Pacific Time (US & Canada)\\\; Tijuana"
    old_name = None
    while old_name != name:
        old_name = name
        name = old_name.replace('\\\\', '\\').replace(r'\;', ';')
    return name

def get_tz_name(tzinfo, dt):
    dt = dt.astimezone(tzinfo)
    name = None
    if hasattr(dt.tzinfo, 'tzname'):
        name = dt.tzinfo.tzname(dt.astimezone(tzinfo))
    if not name and hasattr(dt.tzinfo, '_tzid'):
        name = dt.tzinfo._tzid              # cheating, this is internal to tzical
        name = unescape_ics_tzname(name)
    if not name and hasattr(dt.tzinfo, 'zone'):
        name = tzinfo.zone
    if not name:
        name = str(dt.tzinfo) 
    return name

def format_time(dt, **kwargs):
    use_ampm = kwargs.get('use_ampm', True)
    display_ampm = kwargs.get('display_ampm', True)
    display_tz = kwargs.get('display_tz', False)
    tzinfo = kwargs.get('tzinfo', None)
    if tzinfo:
        dt = dt.astimezone(tzinfo)
    if use_ampm:
        hour = dt.hour % 12
        if hour == 0:
            hour = 12
        if display_ampm:
            result = '%d:%02d%s' % (hour, dt.minute, _('am') if dt.hour < 12 else _('pm'))
        else:
            result = '%d:%02d' % (hour, dt.minute)
    else:
        result = '%d:%02d' % (dt.hour, dt.minute)
    if display_tz and tzinfo:
        result = result + ' ' + get_tz_name(tzinfo, dt)
    return result.strip()
    
def format_date(dt, **kwargs):
    import calendar
    
    display_past_weekdays = kwargs.get('display_past_weekdays', False)
    display_future_weekdays = kwargs.get('display_future_weekdays', False)
    display_tomorrow = kwargs.get('display_tomorrow', False)
    display_yesterday = kwargs.get('display_yesterday', False)
    display_today = kwargs.get('display_today', False)
    display_upper = kwargs.get('display_upper', True)
    display_both = kwargs.get('display_both', False)
    display_day_abbr = kwargs.get('display_day_abbr', False)
    modify_today = kwargs.get('modify_today', False)
    tzinfo = kwargs.get('tzinfo', None)
    if tzinfo:
        dt = dt.astimezone(tzinfo)
    dt = day_start(dt)
    n = day_start(now().astimezone(dt.tzinfo))
    
    result = ''
    if display_today and (dt == n):
        result = _("this") if modify_today else _("today")
    elif display_tomorrow and (dt == n + timedelta(days=1)):
        result = _("tomorrow")
    elif display_yesterday and (dt == n - timedelta(days=1)):
        result = _("yesterday")
    elif display_past_weekdays and (n - timedelta(days=6) <= dt < n):
        result = calendar.day_abbr[dt.weekday()] if display_day_abbr else calendar.day_name[dt.weekday()]
    elif display_future_weekdays and (n < dt <= n + timedelta(days=6)):
        result = calendar.day_abbr[dt.weekday()] if display_day_abbr else calendar.day_name[dt.weekday()]
        
    if (result and display_both):
        result += ' (%d/%d)' % (dt.month, dt.day)
    elif not result:
        result = '%d/%d' % (dt.month, dt.day)
    if display_upper:
        result = result[0].upper() + result[1:]
    return result
        
def format_datetime(dt, **kwargs):
    display_dayonly_if_daystart = kwargs.get('display_dayonly_if_daystart', False)
    tzinfo = kwargs.get('tzinfo', None)
    if (not display_dayonly_if_daystart) or (day_start(dt.astimezone(tzinfo)) != dt.astimezone(tzinfo)):
        return "%s %s" % (format_date(dt, **kwargs), format_time(dt, **kwargs))
    else:
        return format_date(dt, **kwargs)
        
def format_span(span, **kwargs):
    '''Format a span for display (or anything with .start and .end attributes)
    By default, doesn't display a range (but it uses end to determine whether it's an all day event'''
    
    display_range = kwargs.get('display_range', False)
    display_date = kwargs.get('display_date', True)
    display_sometime = kwargs.get('display_sometime', False)
    display_upper = kwargs.get('display_upper', True)  
    display_tz = kwargs.get('display_tz', False)
    display_month_abbr = kwargs.get('display_month_abbr', False)
    kwargs['display_upper'] = False     # don't capitalize the parts we get from format_date
    kwargs['display_tz'] = False        # we'll provide the TZ name
    tzinfo = kwargs.get('tzinfo', None)
    if tzinfo:
        start = span.start.astimezone(tzinfo)
        end = span.end.astimezone(tzinfo)
    else:
        start = span.start
        end = span.end
        
    rstart, rend = round_nearest_day(start), round_nearest_day(end)
    dp = find_day_portion(rstart, rend)
    if dp:
        if display_date:
            if display_sometime:
                kwargs_mod = copy.copy(kwargs)
                kwargs_mod['modify_today'] = dp['modify_today']
                result = "%s %s %s" % (_('sometime'), format_date(rstart, **kwargs_mod), dp['suffix_text'])
            else:
                result = "%s %s" % (format_date(rstart, **kwargs), dp['suffix_text'])
        else:
            result = dp['suffix_text']
    elif is_all_day(rstart, rend):
        if display_date:
            if display_sometime:
                result =  "%s %s" % (_('sometime'), format_date(rstart, **kwargs))
            else:
                result =  "%s %s" % (format_date(rstart, **kwargs), _('all day'))
        else:
            result =  _('all day')
    elif is_all_week(rstart, rend):
        if display_date:
            if display_sometime:
                result =  "%s %s-%s" % (_('sometime week of'), format_date(rstart, tzinfo=tzinfo), format_date(rend, tzinfo=tzinfo))
            else:
                result =  "%s-%s %s" % (format_date(rstart, tzinfo=tzinfo), format_date(rend, tzinfo=tzinfo), _('all week'))
        else:
            result =  _('all week')
    elif is_all_month(rstart, rend):
        if display_date:
            if display_month_abbr:
                name = calendar.month_abbr[rstart.month]
            else:
                name = calendar.month_name[rstart.month]
            if display_sometime:
                result =  "%s %s" % (_('sometime in'), name)
            else:
                result =  "%s %s" % (name, _('all month'))
        else:
            result =  _('all month')    
    else:
        if tzinfo and display_tz:
            start_tz_name = get_tz_name(tzinfo, start)
            end_tz_name = get_tz_name(tzinfo, end)
        else:
            start_tz_name = ''
            end_tz_name = ''
        if display_range:

            if day_start(start) != day_start(end):
                # Starts and ends on two different dates
                if start_tz_name == end_tz_name:
                    result =  "%s - %s %s" % (format_datetime(start, **kwargs), format_datetime(end, **kwargs), start_tz_name)
                else:
                    result =  "%s %s - %s %s" % (format_datetime(start, **kwargs), start_tz_name, format_datetime(end, **kwargs), end_tz_name)
            else:
                # TODO: Suppress "am" or "pm" for the start if it's the same as the end
                if display_date:
                    if start_tz_name == end_tz_name:
                        result =  "%s %s-%s %s" % (format_date(start, **kwargs), format_time(start, **kwargs), format_time(end, **kwargs), start_tz_name)
                    else:
                        result =  "%s %s %s-%s %s" % (format_date(start, **kwargs), format_time(start, **kwargs), start_tz_name, format_time(end, **kwargs), end_tz_name)
                else:
                    if start_tz_name == end_tz_name:
                        result =  "%s-%s %s" % (format_time(start, **kwargs), format_time(end, **kwargs), start_tz_name)
                    else:
                        result =  "%s %s - %s %s" % (format_time(start, **kwargs), start_tz_name, format_time(end, **kwargs), end_tz_name)
        else:
            result =  "%s %s" % (format_datetime(start, **kwargs), start_tz_name)
    
    if display_upper:
        result = result[0].upper() + result[1:]
    return result.strip()


def format_payment_period(num_dmy, dmy, use_abbrevs=False, include_one=False):
    
    result = ''
    if use_abbrevs:
        if dmy == 'D':      result = ungettext("day", "days", num_dmy)
        elif dmy == 'W':    result = ungettext("wk.", "wks.", num_dmy)
        elif dmy == 'M':    result = ungettext("mo.", "mos.", num_dmy) 
        elif dmy == 'Y':    result = ungettext("yr.", "yrs.", num_dmy)  
    else:
        if dmy == 'D':      result = ungettext("day", "days", num_dmy)
        elif dmy == 'W':    result = ungettext("week", "weeks", num_dmy)
        elif dmy == 'M':    result = ungettext("month", "months", num_dmy)
        elif dmy == 'Y':    result = ungettext("year", "years", num_dmy)
        
    if (num_dmy > 1) or include_one:
        result = '%d %s' % (num_dmy, result)
    return result
      
      
def reinterpret_by_tz(dt):
    '''
    Take a datetime with a timezone, and reinterpret it according to the correct
    Daylight Savings Time setting for that date.
    Note: This new datetime is not necessarily equal to the old datetime!
    For example:
    >>> dt
    datetime.datetime(2009, 3, 11, 13, 52, 41, 460647, tzinfo=<DstTzInfo 'US/Central' CST-1 day, 18:00:00 STD>)
    >>> rein(dt)
    datetime.datetime(2009, 3, 11, 13, 52, 41, 460647, tzinfo=<DstTzInfo 'US/Central' CDT-1 day, 19:00:00 DST>)
    >>> dt - rein(dt)
    datetime.timedelta(0, 3600)   
    '''
    
#    import pdb
#    pdb.set_trace()
#    
    if dt.tzinfo is None:
        raise Exception("reinterpret_by_tz requires a time-zone aware datetime")
    if hasattr(dt.tzinfo, 'localize'):
        return dt.tzinfo.localize(dt.replace(tzinfo=None))
    else:
        return dt
        


# Functions for determining time zones from emails, and storing time zones in the DB

def timezone_from_timezonestr(timezonestr, ics=None):
    """Given a timezone string stored in the DB,
    return a tzinfo structure that matches it.
    
    timezone strings can be one of the following:
    
    utc:                    Gives the pytz.UTC timezone
    pytz:<zonename>         Give a pytz timezone based on the name
                            These timezones can account for daylight savings time, etc.
                                Example: "pytz:US/Central"
    ics:<zonename>          Gives the dateutil.tzical timezone in the given ICS file (if any)
                                (<zonename> is ignored for now, assume only one tz in the file)
    tzoffset:<utcoffset>    Returns a dateutil.tz.offset with the given number of seconds
                                Example: "tzoffset:-3600"
    <zonename>              Same as pytz:<zonename>, for backwards compatibility
                                Example: "US/Central"
    """
    
    try:
        if ':' in timezonestr:
            key, value = timezonestr.split(':')
            if key == 'utc':
                return pytz.UTC
            elif key == 'pytz':
                return pytz.timezone(value)
            elif key == 'tzoffset':
                return tz.tzoffset(value, int(value))
            elif key == 'ics':
                if ics:
                    tz_ical = tz.tzical(StringIO.StringIO(str(ics)))
                    return tz_ical.get()
                else:
                    return None
        else:
            return pytz.timezone(timezonestr)
    except:
        return None

                        
def timezonestr_from_utcoffset(utcoffset):
    """Given an offset from UTC in seconds, 
    create a "timezone string" that currently matches that offset,
    suitable for storing in the DB."""

    # This is a quick list of the most "famous" timezones for each UTC offset
    #  that I picked from pytz.common_timezones.
    
    TIMEZONES = [  
        'Pacific/Midway',
        'US/Hawaii',
        'America/Adak',
        'US/Alaska',
        'US/Pacific',
        'US/Mountain',
        'US/Central',
        'US/Eastern',
        'America/Halifax',
        'Atlantic/South_Georgia',
        'Atlantic/Cape_Verde',
        'UTC',
        'Europe/London',
        'Europe/Paris',
        'Europe/Moscow',
        'Asia/Karachi',
        'Asia/Dhaka',
        'Asia/Bangkok',
        'Asia/Singapore',
        'Asia/Tokyo',
        'Australia/Brisbane',
        'Australia/Sydney',
        'Pacific/Fiji'
    ]
    
    if utcoffset is None:
        return 'UTC'
    
    n = now()
    td = timedelta(seconds=utcoffset)
    for zone in TIMEZONES:
        # Note: The order of calling astimezone, utcoffset, etc. is VERY IMPORTANT
        # to account for Daylight Saving Time properly.
        tz = pytz.timezone(zone)
        if n.astimezone(tz).utcoffset() == td:
            return 'pytz:%s' % zone
    
    return 'pytz:UTC'      
    # return 'tzoffset:%d' % utcoffset
    
    
#def zones(hours):
#    n = now()
#    td = timedelta(seconds=hours*3600)
#    for zone in pytz.common_timezones:
#        # Note: The order of calling astimezone, utcoffset, etc. is VERY IMPORTANT
#        # to account for Daylight Saving Time properly.
#        tz = pytz.timezone(zone)
#        if n.astimezone(tz).utcoffset() == td:
#            print 'pytz:%s' % zone


def parsedate_from_email(date_str):
    n = now()
    try:
        timetuple = parsedate_tz(date_str)
        if timetuple:
            utctimestamp = mktime_tz(timetuple)
            dt = datetime.fromtimestamp(utctimestamp, pytz.UTC)
            if dt > n:
                dt = n
            timezone = timezonestr_from_utcoffset(timetuple[-1])
        else:
            log("Couldn't parse date ", date_str)
            dt = n
            timezone = timezonestr_from_utcoffset(None)                     
    except:
        log("Couldn't parse date ", date_str)
        dt = n
        timezone = timezonestr_from_utcoffset(None) 
    return dt, timezone


# Modified from "formatdate" in Python's email.utils
def formatdate_for_email(dt=None):
    """Returns a date string as specified by RFC 2822, e.g.:

    Fri, 09 Nov 2001 01:08:47 -0000

    Same as Python's email.utils.formatdate, except that it takes a 
    datetime and respects the datetime's timezone.
    """
    # Note: we cannot use strftime() because that honors the locale and RFC
    # 2822 requires that day and month names be the English abbreviations.
    if dt is None:
        dt = now()

    tup = dt.timetuple()
    utcoff = dt.utcoffset()
    offset = utcoff.days*(3600*24) + utcoff.seconds
    hours, minutes = divmod(abs(offset), 3600)
    # [Emil] Apparently the signs here need to be the opposite from Python's formatdate.
    if offset > 0:
        sign = '+'
    else:
        sign = '-'
    zone = '%s%02d%02d' % (sign, hours, minutes // 60)

    return '%s, %02d %s %04d %02d:%02d:%02d %s' % (
        ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][tup[6]],
        tup[2],
        ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][tup[1] - 1],
        tup[0], tup[3], tup[4], tup[5],
        zone)
       
 

    
# Convert datetime to/from JavaScript times (milliseconds since the Epoch)
# In Javascript these are given as Date.getTime();
# These MUST be UTC!

# TODO: Use non-naive datetimes [emil]

# def jstime_from_datetime(dt):
#     from calendar import timegm         # This is needed for correct UTC conversion
#     return int(1000 * timegm(dt.utctimetuple()) + dt.microsecond // 1000)
# 
# def datetime_from_jstime(jstime):
#     from pytz import UTC
#     return datetime.fromtimestamp(float(jstime) / 1000.0, tz=UTC).replace(tzinfo=None)

# ================================================
# Name functions

def initial_caps(name):
    """Convert a name to Initial Capitals."""
    
    def title_case(name):
        name = name.title()
        if name.startswith('Mc'):
            name = 'Mc' + name[2:].title()
        elif name.startswith('Mac'):
            name = 'Mac' + name[3:].title()
        return name
        
    parts = name.split('-')
    caps = [title_case(part) for part in parts]
    retval = '-'.join(caps)
    return retval

def get_first_last_name_from_email(addr, use_namelist=True):
    addr_parts = addr.split('@')[0].split('.')
    if len(addr_parts) >= 2:
        return initial_caps(addr_parts[0]), initial_caps(addr_parts[-1])
    else:
        if use_namelist:
            # For parsing email addresses that we get in crawls, direct mails etc.
            namelist = get_wordlist('names_male') + get_wordlist('names_female')
            if addr_parts[0].title() in namelist:
                return initial_caps(addr_parts[0]), ''
            else:
                return '', ''
        else:
            # For parsing assistant email addresses when someone signs up over e-mail
            return initial_caps(addr_parts[0]), ''
        
    
def domain_from_email(email):
    "Given blah@stuff.foobar.com, return foobar.com"
    parts = email.strip().lower().split('@')[-1].split('.')
        
    for i in range(2, len(parts) + 1):
        domain = '.'.join(parts[-i:])
        if domain not in SECOND_LEVEL_DOMAINS:
            return domain
        
    return '.'.join(parts)


def get_company_from_email(addr):
    "From an email address, try to guess the company name (None if no guess)"
    if is_common_domain(addr):              # For gmail.com, etc. don't do the company name
        return None
    domain = domain_from_email(addr)
    return domain.split('.')[0].title()

    
def get_first_last_name(name, addr=None):
    if name:
        name = re.sub('^(\\s|\'|\")*', '', name)    # remove leading spaces and quotes
        name = re.sub('(\\s|\'|\")*$', '', name)    # remove trailing spaces and quotes
        name = re.sub(r'\(.*\)', '', name)          # remove designators like (Sales)
        name = re.sub(r'\{.*\}', '', name)          # ditto
        
        wordlist = get_wordlist('word_beginnings')
        namelist = get_wordlist('names_male') + get_wordlist('names_female')
 
        if RE_EMAIL.match(name):                    # sometimes an email address is given for a name
            return get_first_last_name_from_email(name)
        
        if RE_URL.search(name) and any([x in name.lower() for x in ['.com', '.net', '.org', '.info']]):
            # return name, ''
            return '', ''
        
        if name.count(',') == 1:
            name_parts = name.split(',')
            return initial_caps(name_parts[1].strip()), initial_caps(name_parts[0].strip())
        elif name.count(',') > 1:
            return name, ''
        
        name = name.replace('.', ' ')
        words = name.split(' ')
        
        if any(word.lower()[:6] in wordlist for word in words if len(word) >= 6):
            if not any(word.title() in namelist for word in words if len(word) >= 3):
                return name, ''
        
        if len(words) > 1:    
            return initial_caps(words[0]), initial_caps(words[-1])
        else:
            return initial_caps(words[0]), ''
    else:
        if addr:
            return get_first_last_name_from_email(addr)
        else:
            return '', ''
    
    

def is_special_address(email_address, test_user=True, test_debug=True):
    addr = email_address.lower().split('@')[0]
    domain = email_address.lower().split('@')[-1]
    if Site.objects.filter(domain__icontains=domain).count() > 0:
        if test_user and (addr in settings.NOTED_USER_ADDRESSES):
            return True
        if test_debug and (addr in settings.NOTED_DEBUG_ADDRESSES):
            return True
    return False
    
def is_assistant_address(email_address):
    """
    Determine if an email address belongs to a Noted.cc domain,
    and is not one of the special addresses like emil.gilliam@noted.cc
    (Look at the top-level domain and the second-level domain)
    """
    email_address = email_address.strip().lower()
    if is_special_address(email_address):
        return False
    if email_address and '@' in email_address:
        address_parts = email_address.split('@')
        domain_parts = address_parts[1].split('.')
#        for test_domain in settings.NOTED_DOMAINS:
#            test_domain_parts = test_domain.split('.')
#            if domain_parts[-len(test_domain_parts):] == test_domain_parts:
#                return True
        for site in Site.objects.all():
            test_domain_parts = site.domain.replace(':8000', '').lower().split('.')
            if domain_parts[-len(test_domain_parts):] == test_domain_parts:
                return True
    return False


def normalize_assistant_address(email_address):
    """
    Remove any third-level (or above) domain parts from an assistant email address,
    since we're not using those anymore [eg 12/4]
    """
    email_address = email_address.strip().lower()
    name, domain = email_address.split('@')[0], email_address.split('@')[-1]
    normalized_domain = '.'.join(domain.split('.')[-2:])
    normalized_address = '@'.join([name, normalized_domain])
    return normalized_address 
    

def is_mailer_daemon(email_address):
    if re.search(r'(?i)mailer-daemon|postmaster', email_address):
        return True
    return False
    
def is_ignorable_from_address(addr_space, addr, ignore_mailer_daemon=True):
    '''Don't process an email if its from address is one of these'''
    if not addr:
        return True
    if addr_space == ADDR_SPACE_RFC822:
        if len(addr) > MAX_EMAIL_ADDRESS_LENGTH:         # addresses this long are probably garbage 
            return True
        if is_assistant_address(addr):
            return True
        if is_special_address(addr, test_user=False, test_debug=True):
            return True
        if '@' in addr:                                # if no @, it's a Twitter screen name
            if any ([re.search(pattern, addr) for pattern in settings.IGNORE_INCOMING_DOMAINS]):
                return True    
        if ignore_mailer_daemon and is_mailer_daemon(addr):
            return True
        if re.search(r'(?i)no[^@]*reply|dono?t[^@]*reply|webserver|root@', addr):
            return True
    return False

def is_ignorable_to_address(addr_space, addr):
    if addr_space == ADDR_SPACE_RFC822:
        if any ([re.search(pattern, addr) for pattern in settings.IGNORE_OUTGOING_DOMAINS]):
            return True  
    return False
    
def is_private_address(addr):
    '''Mark an email's events as private if the from address is one of these'''
    addr = addr.split('@')[0]
    if 'admin' in addr.lower():
        return True
    return False

            
# ============================================================
# Unique ID functions


def number_from_bytes(bytes):
    result = 0
    for b in bytes:
        result = 256*result + ord(bytes[i])
    return result
    
def create_slug(number=None, s=string.uppercase, length=5):
    """Create a 5-character uppercase alphabetic identifier.
    TODO: Make this unique within the model that uses it."""
    return ''.join([random.choice(s) for i in range(length)])

def create_uuid():
    """Use this instead of the uuid module to make uuids --
    uuid doesn't always return unique stuff within celery!"""
    return hashlib.md5(create_slug(length=20)).hexdigest()

''' For creating quick TinyURLs: 
    Example: http://snipd.com/~g7YF/
    http://leahculver.com/2008/06/17/tiny-urls-based-on-pk/

url56 = '23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'

def to_url56(num):
    return dec_to_anybase(num, url56)
    
def from_url56(value):
    return anybase_to_dec(value, url56)

# base 10 to any base using basestring for digits 
def dec_to_anybase(num, basestring):
    base = len(basestring)
    new = ''
    current = num
    while current >= base:
        remainder = current % base
        digit = basestring[remainder]
        new = '%s%s' % (digit, new)
        current = current / base
    if basestring[current]: # non-zero indexing
        new = '%s%s' % (basestring[current], new)
    return new

# any base defined by basestring to base 10
def anybase_to_dec(value, basestring):
    base = len(basestring)
    n = 0
    count = 0
    while value:
        last = len(value) - 1
        digit = value[last]
        n += basestring.index(digit) * base**count
        value = value[:last]
        count += 1
    return n

'''
    
# ============================================================
# PayPal

def paypal_timedelta(s):      # str is "7 D", etc.
    num_dmy, dmy = s.split()
    num_dmy = int(num_dmy)
    if dmy == "D":
        return ONE_DAY * num_dmy
    elif dmy == "M":
        return 31*ONE_DAY * num_dmy
    elif dmy == "Y":
        return 366*ONE_DAY * num_dmy
    else:
        log("paypal_timedelta: unknown dmy %s" % dmy)
        return timedelta(0)    

# ============================================================
# names

def random_names(n, selection=None):
    
    male = [(name, 'M') for name in get_wordlist('names_male')]
    female = [(name, 'F') for name in get_wordlist('names_female')]
    
    if selection:
        selection = selection.lower()
        if selection.upper()[0] == 'M':
            names = male
        elif selection.upper()[0] == 'F':
            names = female
    else:
        names = male + female
    
    import random
    result = []
    for i in range(n):
        name = random.choice(names)
        while name in result:
            name = random.choice(names)
        result.append(name)
    return result


# ==============================
# Text utils


def ellipsize_and_more(text, length_min=45, length_max=60, dotdotdot=True, length_with_url_max=80):
    '''Take text, truncate it at a word boundary between index "min" and "max" if possible and add "..."
    '''
    
    if text is None:
        return '', False
        
    text = ' '.join(text.split())
    if len(text) < length_min:
        result = text
        more = False
    elif ' ' in text[length_min:length_max]:
        break_index = text[length_min:length_max].index(' ') + length_min
        result = text[:break_index] + ('...' if dotdotdot else '')
        more = len(text) > break_index
    else:
        found = False
        if length_with_url_max is not None:
            for m in RE_URL.finditer(text):
                if m.start() < length_max and m.end() > length_max: 
                    found = True 
                    # This is a URL that would get truncated
                    if m.end() <= length_with_url_max:
                        # include the whole URL
                        result = text[:m.end()] + ' ' + ('...' if dotdotdot else '')
                    else:
                        # don't include the URL at all
                        result = text[:m.start()] + ('...' if dotdotdot else '')
                    break
        if not found: 
            result = text[:length_max] + ('...' if dotdotdot else '')
        more = len(text) > length_max
        
    return result, more
    
def ellipsize(*args, **kwargs):
    text, more = ellipsize_and_more(*args, **kwargs)
    return text

    
def normalize_subject(subject):
    return ' '.join(subject.split()) 
    

def str_or_none(text):
    return text if text else ''


# ==============================
# Misc.
   
def uniqify(l, remove_nones=True):
    """
    Return a list of the unique items in a list l
    Works for any objects, not just those that can be used in set()
    """
    result = []
    for item in l:
        if remove_nones and (item is None):
            continue
        if item not in result:
            result.append(item)
    return result
    
def combine_lists(lists, remove_dupes=True):
    result = []
    for l in lists:
        result.extend(list(l))
    if remove_dupes:
        result = uniqify(result)
    return result
    
def choose_by_distribution(tuples):
    sum_weights = sum([t[1] for t in tuples])
    r = random.random() * sum_weights
    accum = 0
    for val, weight in tuples:
        accum += weight
        if r <= accum:
            return val
    return tuples[-1][0]
    
def uniqify_by_email(l, remove_nones=True):
    """
    Return a list of aliases in a list l that have distinct email addresses
    """
    result = []
    for item in l:
        if remove_nones and (item is None):
            continue
        if item.email not in [a.email for a in result]:
            result.append(item)
    return result    
    
def remove_nones(l):
    '''
    Return a list of the items in a list that are not None
    '''
    return [x for x in l if (x is not None)]


    # result = proc.communicate()[0].strip()
    # status = proc.returncode
    # if status != 0:
    #     raise Exception("youtube-dl.py: could not extract .flv URL from external_url %s" % external_url)


# Dictionary that 
#  from http://code.activestate.com/recipes/66315/#c8

class CaselessDict(dict):
    '''
    Dictionary that preserves keys in their original case,
    but matches case-insensitively
    from http://code.activestate.com/recipes/66315/#c8
    Useful for things like email headers
    '''
    
    def __init__(self, other=None):
        if other:
            # Doesn't do keyword args
            if isinstance(other, dict):
                for k,v in other.items():
                    dict.__setitem__(self, k.lower(), v)
            else:
                for k,v in other:
                    dict.__setitem__(self, k.lower(), v)

    def __getitem__(self, key):
        return dict.__getitem__(self, key.lower())

    def __setitem__(self, key, value):
        dict.__setitem__(self, key.lower(), value)

    def __contains__(self, key):
        return dict.__contains__(self, key.lower())

    def has_key(self, key):
        return dict.has_key(self, key.lower())

    def get(self, key, def_val=None):
        return dict.get(self, key.lower(), def_val)

    def setdefault(self, key, def_val=None):
        return dict.setdefault(self, key.lower(), def_val)

    def update(self, other):
        for k,v in other.items():
            dict.__setitem__(self, k.lower(), v)

    def fromkeys(self, iterable, value=None):
        d = CaselessDict()
        for k in iterable:
            dict.__setitem__(d, k.lower(), value)
        return d

    def pop(self, key, def_val=None):
        return dict.pop(self, key.lower(), def_val)


def distinct_list_from_queryset(queryset, order_by=None):
    """Given a queryset, returns a distinct list of objects.
    Much faster than list(queryset.distinct()) when we're
    only retrieving a few distinct objects out of a big
    set of redundant objects, because it doesn't have to fetch
    all the fields.
    
    Optional order_by: a single field name or a list of fields.
    """
    
    if queryset:
        queryset = queryset.distinct().values('id')
        ids = [x['id'] for x in queryset]
        queryset = queryset.model.objects.filter(id__in=ids)
        if order_by:
            if type(order_by) is list:
                queryset = queryset.order_by(*order_by)
            else:
                queryset = queryset.order_by(order_by)
        return list(queryset.distinct())
    else:
        return []

    
    
