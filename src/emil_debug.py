# Testing functions for the crawler

import pytz
from crawl.tasks import crawl_task
from utils import *
import pdb, traceback

def test_imap(**kwargs):
    mail_settings = {
        'domain': None,
        'mailbox': None,
        'password': 'squish222',
        'port': 993,
        'server': u'imap.gmail.com',
        'server_type': u'sIMP',
        'service': u'mGMA',
        'tzinfo': pytz.timezone('US/Pacific'),
        'use_ssl': True,
        'username': u'severely.666'
    }

    callback_data = {
        'alias_id': 3
    }
    since = now() - 30*ONE_DAY
    callback_url = 'http://curecrm.coml:8000/api/internal/messages/'

    crawl_task.delay(
        mail_settings,
        since,
        callback_url,
        user_email='severely.666@gmail.com',
        debug=True,
        callback_data=callback_data,
        **kwargs
    )


def test_latest():
    kwargs = {'previous_folders': [{'highest_crawled': None, 'highest_path': u'2004', 'highest_date': now()-ONE_HOUR, 'name': u'[Gmail]/All Mail'}]}
    test_imap(**kwargs)


def test_tasks(num_tasks=10):

    mail_settings = [
        {
            'domain': None,
            'mailbox': None,
            'password': 'squish222',
            'port': 993,
            'server': u'imap.gmail.com',
            'server_type': u'sIMP',
            'service': u'mGMA',
            'tzinfo': pytz.timezone('US/Pacific'),
            'use_ssl': True,
            'username': u'severely.666'
        },
        {
            'domain': None,
            'mailbox': None,
            'password': 'squish222',
            'port': 993,
            'server': u'imap.gmail.com',
            'server_type': u'sIMP',
            'service': u'mGMA',
            'tzinfo': pytz.timezone('US/Pacific'),
            'use_ssl': True,
            'username': u'severely.777'
        },
        {
            'domain': None,
            'mailbox': None,
            'password': 'squish222',
            'port': 993,
            'server': u'imap.gmail.com',
            'server_type': u'sIMP',
            'service': u'mGMA',
            'tzinfo': pytz.timezone('US/Pacific'),
            'use_ssl': True,
            'username': u'severely.666'
        },
        
        {
            'domain': None,
            'mailbox': None,
            'password': 'squish222',
            'port': 993,
            'server': u'imap.gmail.com',
            'server_type': u'sIMP',
            'service': u'mGMA',
            'tzinfo': pytz.timezone('US/Pacific'),
            'use_ssl': True,
            'username': u'severely.666'
        }
    ]

    import random

    for i in range(num_tasks):
        since = now() - 30*ONE_DAY
        crawl_task.delay(random.choice(mail_settings), since, None, user_email='severely.666@gmail.com', debug=True, callback_data=None)
