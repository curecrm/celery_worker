# -*- coding: utf-8 -*-
import os

from celery import Celery

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
WORDLIST_DIR = os.path.join(PROJECT_DIR, 'data')

CELERY_DEFAULT_QUEUE = 'celery_worker'

REDIS_HOST = os.environ.get('CELERY_REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('CELERY_REDIS_PORT', '6379')
REDIS_DB = os.environ.get('CELERY_REDIS_DB', '0')
REDIS_PASSWORD = os.environ.get('CELERY_REDIS_PASSWORD', '')

# TODO: remove this configs
MOSSO_API_USERNAME = 'curecrm'
MOSSO_API_KEY = '90495445e5b6472984761c2f2b5d96b3'
MOSSO_USE_SERVICENET = False        # whether to use internal IP when connecting to Cloud Files


def get_broker_url():
    return 'redis://:{password}@{host}:{port}/{db}'.format(
        password=REDIS_PASSWORD,
        host=REDIS_HOST,
        port=REDIS_PORT,
        db=REDIS_DB)


def get_celery():
    return Celery('celery_worker',
                  backend=get_broker_url(),
                  broker=get_broker_url())
