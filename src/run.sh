#!/bin/bash

export C_FORCE_ROOT=1


CELERY=/home/curecrm/.virtualenvs/celery/bin/celery


case "$1" in
    start)
	$CELERY multi start celery_worker -A crawl.tasks -l info -f /home/curecrm/logs/celery_worker.log
        ;;
    stop)
	$CELERY multi stop celery_worker -A crawl.tasks -l info
        ;;
    debug)
        $CELERY -A crawl.tasks worker -l info
        ;;
    *)
        echo "Usage $0 {start|stop|debug}"
esac

